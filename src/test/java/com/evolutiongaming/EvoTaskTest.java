package com.evolutiongaming;

import com.evolutiongaming.pages.*;
import com.evolutiongaming.util.Settings;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EvoTaskTest {
    private static Site site;

    /**
     * 1. Открыть браузер и развернуть на весь экран.
     * 2. Зайти на ss.lv.
     */
    @BeforeClass
    public static void openSite() {
        site = new Site(Settings.site());
    }

    /**
     * 3. Поменять язык на русский.
     */
    @Test
    public void languageIsSuccessfullyChanged() {
        MainPage mainPage = site.mainPage();
        Assert.assertEquals(mainPage.menuLangTitle(), "RU");

        mainPage.menuLangClick();
        Assert.assertEquals(mainPage.menuLangTitle(), "LV");
        Assert.assertTrue(mainPage.urlContains("/ru/"));
    }

    /**
     * 4. Зайти в раздел Электротехника, в поиске ввести искомую фразу
     *    (напр. ‘Компьютер’ и выбрать разные параметры поиска.
     * 5. Нажать кнопку Искать
     * 6. Отсортировать результаты по цене и выбрать тип сделки ‘продажа’.
     */
    @Test
    public void searchWithConditionsIsWorking() {
        ElectronicsPage electronicsPage = site.mainPage().electronicsClick();
        Assert.assertTrue(electronicsPage.urlContains("/electronics/"));

        SearchPage searchPage = electronicsPage.menuSearchClick();
        Assert.assertTrue(searchPage.urlContains("/electronics/search/"));

        searchPage.setSearchText("Компьютер");
        Double minPrice = 105.5;
        // value produce bug ordering by price
        // Double maxPrice = 149.95;
        Double maxPrice = 249.95;
        searchPage.setMin(minPrice);
        searchPage.setMax(maxPrice);

        // have bug in js for pulldown menu -> don't close
        ResultSearchPage resultSearchPage = searchPage.searchClick();
        Assert.assertTrue("Search button click fail", searchPage.urlContains("/electronics/search-result/"));

        resultSearchPage.orderByPrice().filterBySell();
        Assert.assertTrue("Not filter by sell", searchPage.urlContains("/electronics/search-result/sell/"));

        Double prevPrice = minPrice;
        // have bug for ordering!!!
        for(Double curPrice: resultSearchPage.getResultPriceList()) {
            Assert.assertTrue("Not filter by Min price value", minPrice <= curPrice);
            Assert.assertTrue("Not ordering by Price", prevPrice <= curPrice);
            Assert.assertTrue("Not filter by Max price value", maxPrice >= curPrice);
            prevPrice = curPrice;
        }
    }

    /**
     * 7. Зайти в расширенный поиск.
     * 8. Задать параметр поиска по цене от 0 до 300.
     * 9. Выбрать не менее 3 любых случайных объявлений.
     * 10. Нажать “Добавить выбранные в закладки”.
     * 11. Открыть "Закладки" и проверить, что объявления на странице совпадают с выбранными ранее
     */
    @Test
    public void extendedSearchAndBookmarkIsSuccessfullySaved() {
        SearchPage searchPage = site.mainPage().electronicsClick().menuSearchClick();
        searchPage.setSearchText("Компьютер");

        ResultSearchPage resultSearchPage = searchPage.submit();
        Integer resultCount = resultSearchPage.getResultList().size();
        Assert.assertTrue(resultCount >= 3);

        List<Integer> select = Arrays.asList(2, 0, 1);
        select.forEach(resultSearchPage::selectResult);
        List<String> selectIdList = select.stream()
                .map(resultSearchPage::getResultId)
                .collect(Collectors.toList());

        resultSearchPage.addToBookmark();

        BookmarksPage bookmarksPage = resultSearchPage.menuBookmarksClick();
        Assert.assertTrue("Bookmarks button click fail", searchPage.urlContains("favorites"));

        List<String> bookmarkIdList = bookmarksPage.getBookmarkIdList();
        Assert.assertTrue("Empty bookmark list", bookmarkIdList != null && bookmarkIdList.size() > 0);
        Assert.assertTrue("Bookmark not correctly saved", bookmarkIdList.containsAll(selectIdList));
    }

    /**
     * 12. Закрыть браузер.
     */
    @AfterClass
    public static void closeBrowser() {
        site.close();
    }
}
