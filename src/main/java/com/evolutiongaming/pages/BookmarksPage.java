package com.evolutiongaming.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class BookmarksPage extends AbstractMenu {

    @FindBy(xpath = "//table//tbody//tr[contains(@id, 'tr_')]")
    private List<WebElement> resultList;

    public List<String> getBookmarkIdList() {
        return resultList.stream().map((we) -> we.getAttribute("id")).collect(Collectors.toList());
    }

}
