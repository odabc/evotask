package com.evolutiongaming.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends AbstractMenu {

    @FindBy(id = "ptxt")
    private WebElement searchText;

    @FindBy(xpath = "//input[@class='in3' and contains(@name, 'min')]")
    private WebElement searchMin;

    @FindBy(xpath = "//input[@class='in3' and contains(@name, 'max')]")
    private WebElement searchMax;

    @FindBy(id = "sbtn")
    private WebElement searchButton;

    public void setSearchText(String text) {
        searchText.clear();

/*
        // fix missing character when use sendKeys for string in dynamic refresh document
        for (String ch: text.split("")) searchText.sendKeys(ch);
        List<WebElement> pt = driver.findElements(By.xpath("//table[@id='page_main']//div[@id='preload_txt']//div[contains(@id, 'cmp_')]"));
        if (pt.size() > 0) pt.get(pt.size() / 2).click();
*/
        searchText.sendKeys(text);
    }

    public void setMin(Double min) {
        searchMin.clear();
        searchMin.sendKeys(min.toString());
    }

    public void setMax(Double max) {
        searchMax.clear();
        searchMax.sendKeys(max.toString());
    }

    public ResultSearchPage searchClick() {
        // searchButton.submit(); -> we need test click!!!
        // temporary fix bug in js for pulldown menu -> don't close
        ((JavascriptExecutor) driver).executeScript("document.getElementById('preload_txt').style.display = 'none';");
        searchButton.click();
        return new ResultSearchPage();
    }

    public ResultSearchPage submit() {
        this.searchButton.submit();
        return new ResultSearchPage();
    }
}
