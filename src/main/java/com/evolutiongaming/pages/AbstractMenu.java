package com.evolutiongaming.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractMenu extends AbstractPage {

    @FindBy(className = "menu_lang")
    private WebElement menuLang;

    @FindBy(xpath = "//div[@id='main_table']//b[@class='menu_main']//a[contains(@href, 'search')]")
    private WebElement menuSearch;

    @FindBy(xpath = "//div[@id='main_table']//b[@class='menu_main']//a[contains(@href, 'favorites')]")
    private WebElement menuBookmarks;

    public AbstractMenu() {
        PageFactory.initElements(driver, this);
    }

    public String menuLangTitle() {
        return menuLang.getText();
    }

    public void menuLangClick() {
        menuLang.click();
    }

    public SearchPage menuSearchClick() {
        menuSearch.click();
        return new SearchPage();
    }

    public BookmarksPage menuBookmarksClick() {
        menuBookmarks.click();
        return new BookmarksPage();
    }
}
