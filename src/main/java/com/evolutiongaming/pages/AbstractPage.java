package com.evolutiongaming.pages;

import com.evolutiongaming.webdriver.Driver;
import org.openqa.selenium.WebDriver;

abstract class AbstractPage {
	WebDriver driver = Driver.instance();

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public boolean urlContains(String sample) {
        return getUrl().contains(sample);
    }
}