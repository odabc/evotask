package com.evolutiongaming.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.LinkedList;
import java.util.List;

public class ResultSearchPage extends AbstractMenu {

    @FindBy(xpath = "//tr[@id='head_line']//td[@class='msg_column_td']//a[@class='a18']")
    private WebElement orderByPrice;

    @FindBy(xpath = "//select[@class='filter_sel']")
    private WebElement selectorDialType;

    @FindBy(xpath = "//table//tbody//tr[contains(@id, 'tr_')]")
    private List<WebElement> resultList;

    public ResultSearchPage orderByPrice() {
        orderByPrice.click();
        return new ResultSearchPage();
    }

    public ResultSearchPage filterBySell() {
        //new Select(selectorDialType).selectByIndex(1);
        selectorDialType.findElement(By.xpath("option[contains(@value, 'sell')]")).click();
        return new ResultSearchPage();
    }

    public List<WebElement> getResultList() {
        return resultList;
    }

    public List<Double> getResultPriceList() {
        List<Double> resPriceList = new LinkedList<>();

        for (WebElement res: getResultList()) {
            String[] text = res.findElement(By.xpath("td[@class='msga2-o pp6']//a")).getText().split(" ");
            Double value = Double.valueOf(text[0]);
            resPriceList.add(value);
        }

        return resPriceList;
    }

    public void selectResult(Integer i) {
        getResultList().get(i).findElement(By.xpath("td[@class='msga2 pp0']")).click();
    }

    public String getResultId(Integer i) {
        return getResultList().get(i).getAttribute("id");
    }

    public void addToBookmark() {
        driver.findElement(By.id("a_fav_sel")).click();
        driver.switchTo().activeElement().click();
    }
}
