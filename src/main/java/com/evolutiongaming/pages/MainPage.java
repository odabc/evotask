package com.evolutiongaming.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends AbstractMenu {

    @FindBy(xpath = "//td[@id='td_6']//div[@class='main_head2']")
    private WebElement electronics;

    MainPage(String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);
    }

    public ElectronicsPage electronicsClick() {
        electronics.click();
        return new ElectronicsPage();
    }
}
