package com.evolutiongaming.pages;

import com.evolutiongaming.webdriver.Driver;

public class Site {
    private final String url;

    public Site(String url) {
        this.url = url;
    }

    public MainPage mainPage() {
        return new MainPage(url);
    }

    public void close() {
        Driver.close();
    }
}
