package com.evolutiongaming.webdriver;

import com.evolutiongaming.util.Settings;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class Driver {
    private static final Driver INSTANCE = new Driver();
    private WebDriver driver = null;

    private Driver() {
        Browser browser = Settings.browser();
        String key = "webdriver." + browser.toString() + ".driver";
        String driverPath = Settings.driverPath();

        System.setProperty(key, driverPath);

        switch (browser) {
            case ie:
                driver = new InternetExplorerDriver();
            case chrome:
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--disable-extensions");
                driver = new ChromeDriver(options);
                break;
            case firefox:
                driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static WebDriver instance() {
        return INSTANCE.driver;
    }

    public static void close() {
        if (instance() != null) {
            instance().quit();
        }
    }
}
