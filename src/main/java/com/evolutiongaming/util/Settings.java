package com.evolutiongaming.util;

import com.evolutiongaming.webdriver.Browser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {
    private static Properties properties = new Properties();
    static {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("test.properties")) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String property(String key) {
        return properties.getProperty(key);
    }

    public static Browser browser() {
        return Browser.valueOf(property("browser").toLowerCase());
    }

    public static String site() {
        return property("site");
    }

    public static String driverPath() {
        return property("driver.path");
    }
}
